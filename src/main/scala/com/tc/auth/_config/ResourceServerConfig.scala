package com.tc.auth._config

import org.springframework.beans.factory.annotation.{Autowired, Value}
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.{EnableResourceServer, ResourceServerConfigurerAdapter}
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices

@Configuration
@EnableResourceServer
class ResourceServerConfig extends ResourceServerConfigurerAdapter {
  @Autowired
  private var tokenService: ResourceServerTokenServices = _

  @Value("${security.oauth2.resource-ids}")
  var resourceIds: String = _

  override def configure(resources: ResourceServerSecurityConfigurer): Unit = {
    resources.resourceId(resourceIds)
      .tokenServices(tokenService)
  }

  override def configure(http: HttpSecurity): Unit = {
    http.requestMatchers()
      .and()
      .authorizeRequests()
      .antMatchers("/login").permitAll()
      .antMatchers("/api/**").authenticated()
      .anyRequest().authenticated()
      .and()
      .csrf().disable();
  }
}

package com.tc.auth._config

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.{Bean, Configuration}
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.core.convert.{DefaultDbRefResolver, DefaultMongoTypeMapper, MappingMongoConverter}
import org.springframework.data.mongodb.core.mapping.MongoMappingContext

@Configuration
class MongoConfig  {
  val log = LoggerFactory.getLogger(this.getClass)
  @Autowired
  private var mongoDbFactory: MongoDbFactory = _

  //remove _class
  @Bean
  def mappingMongoConverter: MappingMongoConverter = {
    val converter = new MappingMongoConverter(new DefaultDbRefResolver(mongoDbFactory), new MongoMappingContext())
    converter.setTypeMapper(new DefaultMongoTypeMapper(null))
    converter
  }
}

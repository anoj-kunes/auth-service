package com.tc.auth._config

import com.tc.auth._auth.AppUserDetailsService
import org.springframework.beans.factory.annotation.{Autowired, Value}
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.web.configuration.{AuthorizationServerConfigurerAdapter, EnableAuthorizationServer}
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.security.oauth2.provider.token.{DefaultTokenServices, ResourceServerTokenServices, TokenEnhancer, TokenStore}
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer


@Configuration
@EnableAuthorizationServer
class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
  @Value("${security.oauth2.scopes}")
  var scopes: Array[String] = _
  @Value("${security.oauth2.clientId}")
  var clientId: String = _
  @Value("${security.oauth2.clientSecret}")
  var clientSecret: String = _
  @Value("${security.oauth2.grant-types}")
  var grantTypes: Array[String] = _
  @Value("${security.oauth2.resource-ids}")
  var resourceIds: Array[String] = _
  @Value("${security.oauth2.sign-key}")
  var signKey: String = _
  @Value("${security.oauth2.redirectUri}")
  var redirectUri: String = _

  @Autowired
  private var authenticationManager: AuthenticationManager = _
  @Autowired
  private var passwordEncoder: PasswordEncoder = _
  @Autowired
  private var userDetailsService: AppUserDetailsService = _

  @throws[Exception]
  override def configure(oauthServer: AuthorizationServerSecurityConfigurer): Unit = {
    oauthServer.passwordEncoder(passwordEncoder)
      .tokenKeyAccess("permitAll()")
      .checkTokenAccess("isAuthenticated()")
  }

  @throws[Exception]
  override def configure(configurer: ClientDetailsServiceConfigurer): Unit = {
    configurer.inMemory
      .withClient(clientId)
      .secret(clientSecret)
      .authorizedGrantTypes(grantTypes:_*)
      .scopes(scopes:_*)
      .resourceIds(resourceIds:_*)
      .redirectUris(redirectUri)
      .autoApprove(true)
  }

  @throws[Exception]
  override def configure(endpoints: AuthorizationServerEndpointsConfigurer): Unit = {
    endpoints.tokenStore(jwtTokenStore)
      .authenticationManager(authenticationManager)
      .accessTokenConverter(accessTokenConverter)
  }

  @Bean
  @Primary
  //Making this primary to avoid any accidental duplication with another token service instance of the same name
  def tokenServices: ResourceServerTokenServices = {
    val defaultTokenServices = new DefaultTokenServices
    defaultTokenServices.setTokenStore(jwtTokenStore)
    defaultTokenServices.setTokenEnhancer(tokenEnhancer)
    defaultTokenServices.setAuthenticationManager(authenticationManager)
    defaultTokenServices.setSupportRefreshToken(true)
    defaultTokenServices
  }

  @Bean
  def accessTokenConverter: JwtAccessTokenConverter = {
    val converter = new CustomTokenEnhancer
    converter.setSigningKey(signKey)
    converter
  }

  @Bean
  def tokenEnhancer: TokenEnhancer = {
    val customTokenEnhancer = new CustomTokenEnhancer
    customTokenEnhancer.setSigningKey(signKey)
    customTokenEnhancer
  }

  @Bean
  def jwtTokenStore: TokenStore = new JwtTokenStore(accessTokenConverter)
}

package com.tc.auth._auth

import java.util

import com.tc.auth.mongo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.{UserDetailsService, UsernameNotFoundException}
import org.springframework.stereotype.Service

@Service
class AppUserDetailsService extends UserDetailsService {

  @Autowired
  private var userRepository: UserRepository = _

  override def loadUserByUsername(username: String): AppUserDetails = {
    val optionalUser = userRepository.findByEmail(username)
    if(!optionalUser.isPresent) new UsernameNotFoundException(s"The username ${username} doesn't exist")
    val appUserDetails = new AppUserDetails(optionalUser.get.email, optionalUser.get.password, new util.ArrayList[SimpleGrantedAuthority]())
    appUserDetails.id = optionalUser.get.id
    appUserDetails.fullName = optionalUser.get.firstName + " " + optionalUser.get.lastName
    appUserDetails.authorityList = this.getAuthorities()

    appUserDetails;
  }

  def getAuthorities(): util.ArrayList[SimpleGrantedAuthority] = {
    val authorities = new util.ArrayList[SimpleGrantedAuthority]();
    authorities.add((new SimpleGrantedAuthority("ROLE_ADMIN")))
    authorities.add((new SimpleGrantedAuthority("ROLE_USER")))
    authorities;
  }
}

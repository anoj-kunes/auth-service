package com.tc.auth.mongo.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
class UserHasAuthority extends Metadata {
  @Id
  var id: String = _;
  var authorityId: String = _;
  var userId: String = _;
}


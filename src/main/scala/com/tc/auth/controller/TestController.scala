package com.tc.auth.controller

import com.tc.auth._auth.AppUserDetails
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, RestController}

@RestController
@RequestMapping(value = Array("${app.api.url}/results"))
class TestController {

  @GetMapping
  def getResults(@AuthenticationPrincipal userDetails: AppUserDetails): ResponseEntity[String] = {
    new ResponseEntity[String](userDetails.getUsername, HttpStatus.OK)
  }
}
